# -*- coding: UTF-8 -*-
from math import sqrt
import matplotlib.pyplot as plt
import scipy.stats as ss


def countData(data):
    # 统计data中各元素的个数，返回2个数组
    # 数组1对应数据中出现的所有不同元素
    # 数组2对应各元素出现次数
    elements = []
    count = []
    for elem in data:
        if elem not in elements:
            elements.append(elem)
            count.append(1)
        else:
            for i in range(0, len(elements)):
                if elements[i] == elem:
                    count[i] += 1
                    break
    return elements, count


def mean(data, format="a"):
    # 返回平均数（默认为算数平均数）
    # 参数data为数据列表
    # 参数format为平均数的类型（默认为算术平均数）
    # format参数说明：
    # a : 算数平均数
    # g : 几何平均数
    # h : 调和平均数
    # q : 平方平均数

    # 初始化
    # 记录数据长度
    dataLength = len(data)
    # 记录总和
    fSum = 0.0

    # 分情况计算平均数

    # 算数平均数
    if format == "a":
        for i in range(0, dataLength):
            fSum += data[i]
        return fSum / dataLength

    # 几何平均数
    elif format == "g":
        # 几何平均数sum初值为1
        fSum = 1.0
        for i in range(0, dataLength):
            fSum *= data[i]
        return fSum ** dataLength

    # 调和平均数
    elif format == "h":
        for i in range(0, dataLength):
            fSum += 1.0 / data[i]
        return 1.0 / fSum

    # 平方平均数
    elif format == "q":
        for i in range(0, dataLength):
            fSum += data[i] ** 2
        return sqrt(fSum / dataLength)


def var(data, isSample=False):
    # 求一组数据的方差
    # data为数据列表
    # isSample指示是否为样本数据
    # 返回值为数据的方差/样本方差
    fSum = 0
    fDataMean = mean(data)
    n = len(data)

    for i in range(0, n):
        fSum += (data[i] - fDataMean) ** 2
    if isSample:
        return fSum / (n - 1)
    else:
        return fSum / n


def std(data, isSample=False):
    # 求一组数据的标准差
    # data为数据列表
    # isSample指示是否为样本数据
    # 返回值为数据的标准差/样本标准差
    return sqrt(var(data, isSample))


def quantile(data, position):
    # 计算分位数
    # data为数据列表
    # position表示分位数的位置
    # 返回值为分位数的值
    n = len(data)
    p = 1.0 * position * n
    data.sort()
    if p == int(p):
        return (data[int(p)] + data[int(p - 1)]) / 2
    else:
        return data[int(p)]


def outlierHandling(data, alpha=1.5, draw=False):
    # 根据四分位数间距进行异常值处理
    # data为数据列表
    # alpha为确定异常值的基准系数
    # draw表示是否画出修正箱线图
    # q1、q3分别为第一、三四分位数
    dataCopy = data
    dataCopy.sort()
    q1 = quantile(dataCopy, 0.25)
    q3 = quantile(dataCopy, 0.75)

    # 四分位间距isq
    isq = q3 - q1

    for elem in dataCopy:
        # 删除异常值
        if elem > q3 + alpha * isq or elem < q1 - alpha * isq:
            dataCopy.remove(elem)

    if draw:
        plt.boxplot(dataCopy)
        plt.show()
    return dataCopy


def confidenceInterval(data, predictElem, alpha=0.05, sigma=None):
    # 计算总体均值、标准差的置信区间
    # data为样本数据
    # predictElem为待估参数:
    # "m" ~ 均值
    # "s" or "v" ~ 标准差
    # alpha为（1-置信水平），默认值为0.05
    # sigma表示总体的标准差（默认为总体标准差未知）
    # 返回值为置信下限和置信上限组成的列表

    # 首先记录一些必要参数
    dataMean = mean(data)  # 样本均值
    dataStd = std(data, isSample=True)  # 样本标准差
    dataLen = len(data)  # 数据个数

    # 按要求对不同待估参数进行计算
    # 待估参数为总体均值的情况：
    if predictElem == "m":

        # 总体标准差未知
        if sigma is None:
            tParameter = abs(ss.t.pdf(alpha / 2, dataLen - 1))
            confidenceLowerLimit = dataMean - dataStd * tParameter / sqrt(dataLen)
            confidenceUpperLimit = dataMean + dataStd * tParameter / sqrt(dataLen)
            return [confidenceLowerLimit, confidenceUpperLimit]

        # 总体标准差已知
        else:
            normParameter = abs(ss.norm.pdf(alpha / 2))
            confidenceLowerLimit = dataMean - sigma * normParameter / sqrt(dataLen)
            confidenceUpperLimit = dataMean + sigma * normParameter / sqrt(dataLen)
            return [confidenceLowerLimit, confidenceUpperLimit]

    # 待估参数为总体标准差的情况：
    elif predictElem == "v" or predictElem == "s":
        chi2Parameter = [ss.chi2.pdf(alpha / 2, dataLen - 1), ss.chi2.pdf(1 - alpha / 2, dataLen - 1)]
        confidenceLowerLimit = sqrt(dataLen - 1) * dataStd / chi2Parameter[1]
        confidenceUpperLimit = sqrt(dataLen - 1) * dataStd / chi2Parameter[0]
        return [confidenceLowerLimit, confidenceUpperLimit]
